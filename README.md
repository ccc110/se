# 陳鍾誠的《軟體工程與演算法》課程

欄位          | 說明
--------------|------------------------
學校科系       | [金門大學](https://www.nqu.edu.tw/) / [資訊工程系](https://www.nqu.edu.tw/educsie/)
開課教師       | [陳鍾誠](https://www.nqu.edu.tw/educsie/index.php?act=blog&code=list&ids=4)
程式語言       | [JavaScript (JS)](js) + [C](c)
難度           | 中等，修本門課者應至少已經學會一種程式語言。
教材           | [軟體工程](./se/_doc/README.md) + [演算法](./alg/_doc/README.md)

