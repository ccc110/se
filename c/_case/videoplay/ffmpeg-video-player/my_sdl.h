#include <SDL2/SDL.h>
#include <SDL2/SDL_video.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_timer.h>
#include <SDL2/SDL_thread.h>

#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#ifdef __WINDOWS__
#define SDL_main WinMain
#endif